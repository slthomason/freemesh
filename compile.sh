#!/bin/bash

name="$0";
boardsdir="./boards";
jobs=$(nproc)

show_boards() {
	echo -n "Available board names:";
	local itr=0;
	for l in $(ls -1 $boardsdir/*.config|sed -e 's/\.config//g'|sed -e 's/.*\///g');do
		if [ $itr == "0" ];then
		    echo ; echo "     ";
		    itr=5;
		fi;
		echo -n "$l ";
		itr=$(expr $itr - 1);
	done
	echo;echo;
};

show_usage() {
	echo;
	echo "USAGE:";
	echo "     $name [command or boardname]";
	echo ;
	echo "Where [command] can be one of:";
	echo "     list       to list available boards configs to compile.";
	echo "     help       to show this help.";
	echo "     usage      to show this help.";
	echo ;
	echo "Example:";
	echo "     $name list";
	echo "  or";
	echo "     $name zbt8103ax_router jobs";
	echo;
};

compile() {
	local build_date=$(date +"%Y-%m-%d_%H-%M-%S")
	local boardname="$1";
	local configfile="$boardsdir/$boardname.config";
	local jobs="$2";
	if [ ! -f "$configfile" ];then
		echo;
		echo "   No such config file found. Use 'list' argument to see available configs.";
		echo;
		return;
	fi
	
	git config --global http.postBuffer 500M
	git config --global http.maxRequestBuffer 100M
	git config --global core.compression 0

	#REPO_URL="git@github.com:openwrt/openwrt.git"
	REPO_URL="https://github.com/openwrt/openwrt.git"
	REPO_VERSION="v23.05.5"
	REPO_DIR="output_build"
	if [ ! -d "$REPO_DIR" ]; then
		echo "Directory '$REPO_DIR' does not exist. Cloning repository..."
		git clone "$REPO_URL" $REPO_DIR
		cd $REPO_DIR && git checkout $REPO_VERSION && cd ..
	else
		echo "Directory '$REPO_DIR' already exists. Skipping clone and will do fetch."
		cd $REPO_DIR && git fetch && cd ..
	fi
	cp -rf "$configfile" ./$REPO_DIR/.config;

	cd $REPO_DIR;
	./scripts/feeds update -a && ./scripts/feeds install -a
	if [ ! -d "package/freemesh" ]; then
		mkdir package/freemesh 
	fi
	rm -rf package/freemesh/*
	cp -rf ../freemesh/* package/freemesh
	for patch in ../patches/*; do
		if patch --dry-run -p1 < "$patch" >/dev/null 2>&1; then
		echo "Applying patch $patch..."
		patch -p1 < "$patch"
		else
		echo "Skipping patch $patch as it's already applied."
		fi
	done	
    yes "" | make oldconfig;
	make download -j "$jobs";
	make world -j "$jobs";

	bin_upgrade="mediatek-filogic-zbtlink_zbt-z8103ax-squashfs-sysupgrade.bin"
	bin_output="firmware-$build_date.bin"
	if [ "$boardname" = "zbt8103ax_router" ]; then
		cp -rf "bin/targets/mediatek/filogic/openwrt-$bin_upgrade" "../builds_by_version/router-$bin_output"
		echo "The $boardname firmware file :router-$bin_output will be under builds_by_version folder";

	elif [ "$boardname" = "zbt8103ax_node" ]; then
		cp -rf "bin/targets/mediatek/filogic/openwrt-$bin_upgrade" "../builds_by_version/node_$bin_output"
		echo "The $boardname firmware file :node-$bin_output will be under builds_by_version folder";
	fi
		echo "   Done.";
};

if [ X"$1" = X"" ]; then
	show_usage;
	exit 0;
fi

case "$1" in
list)
	show_boards;
	;;
help|usage)
	show_usage;
	;;
*)
	compile "$1" "$2";
	;;
esac
